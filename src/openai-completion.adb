with Ada.Wide_Wide_Text_IO;
with Ada.Command_Line;
with Ada.Exceptions;
with Ada.IO_Exceptions;
with Ada.Strings.Unbounded;
with Ada.Strings.UTF_Encoding.Wide_Wide_Strings;

with GNAT.Command_Line;

with Util.Http.Clients.Curl;
with Util.Http.Clients;
with Util.Properties;
with Util.Log.Loggers;

with OpenAPI;
with OpenAPI.Clients;
with OpenAPI.Credentials.OAuth;

with OpenAI.Clients;
with OpenAI.Models;
procedure OpenAI.Completion is

   use Ada.Wide_Wide_Text_IO;
   use Ada.Strings.Unbounded;
   use GNAT.Command_Line;

   procedure Usage;
   function Get_Api_Key return String;
   procedure Initialize;

   Error : exception;

   Log       : constant Util.Log.Loggers.Logger
     := Util.Log.Loggers.Create ("OpenAI.Completion");
   Server    : constant OpenAPI.Ustring
     := OpenAPI.To_UString ("https://api.openai.com/v1");
   Arg_Count : constant Natural := Ada.Command_Line.Argument_Count;
   Config    : Util.Properties.Manager;

   procedure Usage is
   begin
      Put_Line ("Usage: openai-completion [-v] [-m model] {question}...");
      Put_Line ("-v        Verbose execution");
      Put_Line ("-m model  The OpenAI model to use (gpt-3.5-turbo, text-davinci-003)");
      Put_Line ("question  Question asked to the OpenAI model");
   end Usage;

   function Get_Api_Key return String is
      Config    : Util.Properties.Manager;
   begin
      Config.Load_Properties ("openai-key.properties");
      return Config.Get ("openai_key");

   exception
      when E : Ada.Io_Exceptions.Name_Error =>
         Log.Error ("No API key: " & Ada.Exceptions.Exception_Message (E));
         raise Error;

   end Get_Api_Key;

   procedure Initialize is
   begin
      begin
         Config.Load_Properties ("openai.properties");

      exception
         when Ada.Io_Exceptions.Name_Error =>
            null;
      end;
      Util.Log.Loggers.Initialize (Config);
   end Initialize;

   function To_WString (S : OpenAPI.Ustring) return Wide_Wide_String is
      Utf8 : constant String := OpenAPI.To_String (S);
   begin
      return Ada.Strings.UTF_Encoding.Wide_Wide_Strings.Decode (Utf8);
   end To_WString;

   First       : Natural := 0;
   Model       : Openapi.Ustring := OpenAPI.To_UString ("text-davinci-003");
begin
   Initialize;
   Initialize_Option_Scan (Stop_At_First_Non_Switch => True);
   loop
      case Getopt ("* v m:") is
         when ASCII.NUL =>
            exit;

         when 'v' =>
            Config.Set ("log4j.logger.OpenAI.Completion", "INFO");
            Config.Set ("log4j.appender.console.level", "INFO");
            Util.Log.Loggers.Initialize (Config);

         when 'm' =>
            Model := OpenAPI.To_UString (Parameter);
            First := First + 1;

         when '*' =>
            exit;

         when others =>
            null;
      end case;
      First := First + 1;
   end loop;
   First := First + 1;
   if First > Arg_Count then
      Usage;
      return;
   end if;
   Util.Http.Clients.Curl.Register;
   declare
      Api_Key : constant String := Get_Api_Key;
      Prompt  : OpenAPI.UString;
      Cred    : aliased OpenAPI.Credentials.OAuth.OAuth2_Credential_Type;
      C       : OpenAI.Clients.Client_Type;
      Req     : OpenAI.Models.CompletionRequest_Type;
      Reply   : OpenAI.Models.CompletionResponse_Type;
   begin
      for I in First .. Arg_Count loop
         if I /= First then
            Append (Prompt, " ");
         end if;
         Append (Prompt, Ada.Command_Line.Argument (I));
      end loop;

      Log.Info ("Create completion '{0}'", Prompt);
      Cred.Bearer_Token (Api_Key);
      C.Set_Server (Server);
      C.Set_Credentials (Cred'Unchecked_Access);

      Req.Model := Model;
      Req.Prompt := (Is_Null => False, Value => Prompt);
      Req.Temperature := 0.3;
      Req.Max_Tokens := (Is_Null => False, Value => 1000);
      Req.Top_P := 1.0;
      Req.Frequency_Penalty := 0.0;
      Req.Presence_Penalty := 0.0;
      C.Create_Completion (Req, Reply);

      for Choice of Reply.Choices loop
         if not Choice.Text.Is_Null then
            Put_Line (To_WString (Choice.Text.Value));
         end if;
      end loop;

   exception
      when Constraint_Error =>
         Put_Line ("Constraint error raised");

      when OpenAPI.Clients.Not_Found =>
         Log.Error ("Completion failed");

   end;

exception
   when Error =>
      Ada.Command_Line.Set_Exit_Status (1);

   when Invalid_Parameter =>
      Usage;
      Ada.Command_Line.Set_Exit_Status (2);

end OpenAI.Completion;
