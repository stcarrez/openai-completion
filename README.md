# Ada OpenAI Completion

General purpose questions to [OpenAI](https://openai.com/) models by using [Ada OpenAI](https://gitlab.com/stcarrez/ada-openai) library.

## Setup and build

Use Alire to setup and build the project:

```
git clone https://gitlab.com/stcarrez/openai-completion.git
cd openai-completion
alr build
```

## OpenAI Access Key

The [OpenAI](https://openai.com/) service uses an OAuth bearer API key
to authenticate requests made on the server.  For the credential setup you will first need to get your access key
from your [OpenAI account](https://platform.openai.com/login).
Once you have your key, create the file `openai-key.properties` file with the command:

```
echo "openai_key=XXXXX" > openai-key.properties
```

## Asking questions

After building and getting the `openai-key.properties` file setup, launch the generation tool and
give a description of the image you want to generate:

```
./bin/openai-completion write an Ada function to generate a prime number
```

You can even write your question in French or another language and get the result in your native tongue:

```
./bin/openai-completion "écrit un discours d'introduction pour ouvrir une journée de présentation au FOSDEM"
```


## Links

- [OpenAI API reference](https://platform.openai.com/docs/api-reference)
- [Ada OpenAI](https://gitlab.com/stcarrez/ada-openai)

